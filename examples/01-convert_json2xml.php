#!/usr/bin/php
<?php
include("../autoload.php");

echo "\nIniciando el programa...\n\n";

$file= "data_v6.json";
$format= "array";

if( !file_exists($file) ) {
	echo "\nEl archivo <". $file. "> no existe";
}
else {
	$jsonData= json_decode(file_get_contents($file), true);
	$c= new JSON2XML($format, $jsonData);
	$c->convert();

	if( $c->getError() ) {
		echo "\nError: ". $c->getError();
	}
	else {
		$r= $c->getRespuesta();
		// echo "\n".$r;

		$doc= $c->getDocument();
		echo "\nDocumento:\n";
		print_r($doc);
		echo "\n";

		// $fileSave= 'out.xml';
		// $c->getDocument($fileSave);
		// echo "\nDocumento:\n";
		// print_r(file_get_contents($fileSave));
		// echo "\n";
	}
}

echo "\n\nFin del programa...\n\n";
exit(0);
?>