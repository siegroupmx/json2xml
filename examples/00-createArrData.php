#!/usr/bin/php
<?php
// $file= "data_v5.json";
// $data= array(
// 	"libros"=>array(
// 		"libro"=>array(
// 			array(
// 				"nombre"=>"Harry Potter", 
// 				"paginas"=>200, 
// 				"fecha"=>2020, 
// 				"creador"=>"Pedro"
// 			), 
// 			array(
// 				"nombre"=>"Senor de los Anillos", 
// 				"paginas"=>500, 
// 				"fecha"=>2000, 
// 				"creador"=>"Luis"
// 			)
// 		)
// 	)
// );
$file= "data_v6.json";
$data= array(
	"libro"=>array(
		array(
			"nombre"=>"Harry Potter", 
			"paginas"=>200, 
			"fecha"=>2020, 
			"creador"=>"Pedro"
		), 
		array(
			"nombre"=>"Senor de los Anillos", 
			"paginas"=>500, 
			"fecha"=>2000, 
			"creador"=>"Luis"
		)
	)
);
$jsonData= json_encode($data);

$fp= fopen($file, "w");
fwrite($fp, $jsonData);
fclose($fp);
unset($fp);

echo "\n\nArr creado...\n\n";
exit(0);
?>