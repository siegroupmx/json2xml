<?php
class JSON2XML{
	private $sucess=NULL;
	private $error=NULL;
	private $error_code=NULL;
	private $xml=NULL;
	private $sourceFormat=NULL;
	private $supporterFormats= array("json", "array");
	private $sourceData=NULL;

	/**
	* establece la informacion de orien
	* @param string $a la informacion de origen
	*/
	public function setSourceData($a=NULL) {
		$this->sourceData= $a;
	}

	/**
	* devuelve la informacion de orien
	* @return string la informacion de origen
	*/
	public function getSourceData($a=NULL) {
		return $this->sourceData;
	}

	/**
	* devuelve la validacion sobre la existencia de la informacion
	* @return boolean falso o verdadero
	*/
	public function existSourceData() {
		$r= false;
		if( $this->isArrayData() ) {
			$r= (is_array($this->getSourceData()) ? true:false);
		}
		else if( $this->isJsonData() ) {
			$r= ((is_object($this->getSourceData()) || is_string($this->getSourceData())) ? true:false);
		}

		return $r;
	}

	/**
	* establece el tipo de formato de la informacion de origen
	* @param string $a puede ser array o json
	*/
	public function setSourceFormat($a=NULL) {
		$r= false;
		foreach( $this->supporterFormats as $k ) {
			if( !strcmp($a, $k) )
				$r= true;
		}

		$this->sourceFormat= ($r ? $a:NULL);
	}

	/**
	* devuelve la informacion origen resguardada
	* @return string puede ser array o json
	*/
	public function getSourceFormat() {
		return $this->sourceFormat;
	}

	/**
	* devuelve verificacion si la data de origen fue indicada como array
	* @return boolean falso o verdadero
	*/
	public function isArrayData() {
		return (!strcmp($this->getSourceFormat(), "array") ? true:false);
	}

	/**
	* devuelve verificacion si la data de origen fue indicada como json
	* @return boolean falso o verdadero
	*/
	public function isJsonData() {
		return (!strcmp($this->getSourceFormat(), "json") ? true:false);
	}

	/**
	* establece el codigo de exito
	*/
	public function setSucess($a=NULL) {
		$this->sucess= $a;
	}

	/**
	* retorna el mensaje de exito
	*/
	public function getSucess() {
		return $this->sucess;
	}

	/**
	* establece el mensaje de error
	*
	* @param string el mensaje de error
	* @param string el codigo de error
	*/
	public function setError($a=NULL, $b=NULL) {
		$this->error= $a; # mensaje de error
		$this->setErrorCode($b); # codigo de error
	}

	/**
	* establece el codigo de error
	*
	* @param string el codigo del error
	*/
	public function setErrorCode($a=NULL) {
		$this->error_code= $a;
	}

	/**
	* retorna el error con codigo
	*
	* @return string error con codigo
	*/
	public function getError() {
		if( is_array($this->error) ) {
			return $this->error;
		}
		else {
			return ($this->error_code ? $this->error_code. ' - ':'').($this->error ? $this->error:'');
		}
	}

	/**
	* retorna la respuesta
	*
	* @return string el mensaje de exito
	*/
	public function getRespuesta() {
		return $this->getSucess();
	}

	/**
	* carga argumentos a una etiqueta
	*
	* @param ChildNode puntero al nodo o etiqueta
	* @param array arreglo de datos que confomaran los argumentos del nodo o etiqueta
	*/
	public function cargaAtt(&$nodo, $datos ) {
		foreach ($datos as $key => $val) {
			$val = preg_replace('/\s\s+/', ' ', $val);   // Regla 5a y 5c
			$val = trim($val);                           // Regla 5b
			
			if (strlen($val)>0) {   // Regla 6
				$val = utf8_encode(str_replace("|","/",$val)); // Regla 1
				$nodo->setAttribute($key,$val);
			}
		}
	}

	public function validateHijoRango($a=NULL) {
		foreach( $a as $k=>$v ) {
			if( is_numeric($k) )
				return true;
		}
		return false;
	}

	public function createXmlfromArray($nodoPadre=NULL, $nodoHijo=NULL, $arbol=NULL) {
		if( is_array($arbol) ) {
			foreach( $arbol as $k=>$v ) {
				if( is_array($v) ) {
					if( !strcmp($k, "@attribute") ) {
						$tmpAttr=array();
						foreach($v as $k2=>$v2 ) {
							$tmpAttr[$k2]= $v2;
						}
						$this->cargaAtt($nodoPadre, $tmpAttr);
					}
					else if( is_numeric($k) ) {
						$a= $this->xml->createElement($nodoHijo);
						$nodoPadre->appendChild($a);
						foreach($v as $k2=>$v2 ) {
							$b= $this->xml->createElement($k2, $v2);
							$a->appendChild($b);
							unset($b);
						}
						unset($a);
					}
					else {
						if( $this->validateHijoRango($v) ) {
							$this->createXmlfromArray($nodoPadre, $k, $v);
						}
						else {
							$a= $this->xml->createElement($k);
							$nodoPadre->appendChild($a);
							$this->createXmlfromArray($a, $k, $v);
						}
						unset($a);
					}
				}
				else {
					if( !strcmp($k, "@value") ) {
						$nodoPadre->textContent= $v;
					}
					else {
						$a= $this->xml->createElement($k, $v);
						$nodoPadre->appendChild($a);
						unset($a);
					}
				}
			}
		}

		return true;
	}

	public function convertDataJsonToObjectJson() {
		if( is_string($this->getSourceData()) ) {
			$this->setSourceData(json_decode($this->getSourceData(), true));
		}
	}

	public function createXmlfromJson($nodoPadre=NULL, $nodoHijo=NULL, $arbol=NULL) {
		$this->convertDataJsonToObjectJson(); // prevent string data
		return $this->createXmlfromArray($nodoPadre, $nodoHijo, $this->getSourceData());
	}

	/**
	* genera la conversion del formato
	*/
	public function convert() {
		if( !$this->getSourceFormat() )
			$this->setError("no indico el tipo de formato de la informacion de origen");
		else if( !$this->existSourceData() )
			$this->setError("no indico se indico la informacion de origen");
		else {
			if( $this->isArrayData() ) {
				if( !$this->createXmlfromArray($this->xml, $this->xml, $this->getSourceData()) ) {
					$this->setError("problemas para crear xml");
					$this->setSucess(NULL);
				}
				else {
					$this->setError(NULL);
					$this->setSucess("documento convertido con exito");
					// print_r($this->xml->saveXML());
				}
			}
			else if( $this->isJsonData() ) {
				if( !$this->createXmlfromJson($this->xml, $this->xml, $this->getSourceData()) ) {
					$this->setError("problemas para crear xml");
					$this->setSucess(NULL);
				}
				else {
				}
			}
		}
	}

	/**
	* proporciona el archivo XML resultante
	* 
	* @param string $a ruta de guardado en caso de requerirse
	*/
	public function getDocument($a=NULL) {
		if( $a ) {
			$this->xml->encoding= 'UTF-8';
			$this->xml->xmlStandalone=false;
			$this->xml->save($a);
		}
		else {
			return $this->xml->saveXML();
		}
	}

	/**
	* constructor
	*/
	public function __construct($srcFormat=NULL, $dataSrc=NULL) {
		$this->setSourceFormat($srcFormat);
		$this->setSourceData($dataSrc);
		$this->xml= new DOMdocument( "1.0", "UTF-8" );
	}
}
?>